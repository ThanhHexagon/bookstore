﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookStore.Models;
namespace BookStore.Controllers
{
    public class CartController : Controller
    {
        private const string CartSession = "CartSession";
        OnlineLibraryEntities db = new OnlineLibraryEntities();
        // GET: Cart
        public ActionResult ViewCart()
        {
            var cart = Session[CartSession];
            var list = new List<CartItem>();

            if (cart != null)
            {
                list = (List<CartItem>)cart;

            }
            return View(list);
        }

        public ActionResult AddItem(int masp, int quantity, string strUrl)
        {
            var sp = db.tblBooks.Where(n => n.BookID == masp).SingleOrDefault();
            var cart = Session[CartSession];
            if (cart != null)
            {
                var list = (List<CartItem>)cart;
                if (list.Exists(x => x.sID == masp))
                {
                    foreach (var item in list)
                    {
                        if (item.sID == masp)
                        {
                            item.iQuantity += quantity;
                        }
                    }
                }
                else
                {
                    // tao moi Cart item
                    var item = new CartItem(masp, quantity);
                    list.Add(item);
                }
                return Redirect(strUrl);
            }
            else
            {
                // tao moi Cart item
                var item = new CartItem(masp, quantity);
                var list = new List<CartItem>();
                list.Add(item);
                // gan vao session
                Session[CartSession] = list;
            }
            return Redirect(strUrl);
        }

        public ActionResult EditCart()
        {
            var cart = Session[CartSession] ;
            if (cart == null)
            {
                return RedirectToAction("Index", "Product");
            }
            var lstShoppingCart = (List<CartItem>)cart;
            return View(lstShoppingCart);
        }

        public ActionResult UpdateCart(int sMaSP, FormCollection f)
        {
            var cart = Session[CartSession];
            var lstShoppingCart = (List<CartItem>)cart;
            CartItem sanpham = lstShoppingCart.Find(n => n.sID == sMaSP);
            if (sanpham != null)
            {
                sanpham.iQuantity = int.Parse(f["quantity"].ToString());
            }
            return RedirectToAction("ViewCart", "Cart");
        }

        public ActionResult DeleteItem(int masp)
        {
            var cart = Session[CartSession];
            var lstShoppingCart = (List<CartItem>)cart;
            CartItem sanpham = lstShoppingCart.Find(n => n.sID == masp);
            if (sanpham != null)
            {
                lstShoppingCart.RemoveAll(n => n.sID == masp);
            }
            if (lstShoppingCart.Count == 0)
            {
                return RedirectToAction("Index", "Product");
            }
            return RedirectToAction("ViewCart", "Cart");
        }

        public float SumTotalMoney()
        {
            float totalMoney = 0;
            var cart = Session[CartSession];
            var lstShoppingCart = (List<CartItem>)cart;

            if (lstShoppingCart != null)
            {
                totalMoney = lstShoppingCart.Sum(n => n.fPrice);
            }
            return totalMoney;
        }

        [HttpPost]
        public ActionResult Checkout()
        {
            if (Session["Account"] == null)
            {
                return RedirectToAction("LoginAccount", "Account");
            }
            if (Session[CartSession] == null)
            {
                return RedirectToAction("Index", "Product");
            }
            tblOutput bill = new tblOutput();
            tblCustomer kh = (tblCustomer)Session["Account"];
            var cart = Session[CartSession];
            var lstShoppingCart = (List<CartItem>)cart;

            bill.CustomerID = kh.CustomerID;
            bill.OutputDate = DateTime.Now;
            db.tblOutputs.Add(bill);
            db.SaveChanges();
            tblOutputDetail billDetail = new tblOutputDetail();
            foreach (var item in lstShoppingCart)
            {               
                billDetail.BookID = item.sID;
                billDetail.OutputID = bill.OutputID;
                billDetail.Quantity = item.iQuantity;             
            }
            billDetail.TotalPay = SumTotalMoney();
            db.SaveChanges();
            Session["ShoppingCart"] = null;
            return RedirectToAction("Index", "Product");
        }
    }
}