﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookStore.Models;
using PagedList;
using PagedList.Mvc;
namespace BookStore.Controllers
{
    public class ProductController : Controller
    {
        OnlineLibraryEntities db = new OnlineLibraryEntities();
        // GET: Product
        // Index view
        public ActionResult Index()
        {
            return View();
        }
        // categories partialview
        public ActionResult Categories()
        {
            var categories = db.tblCategories.OrderBy(n => n.CategoryName).ToList();
            return PartialView(categories);
        }

        public ActionResult Authors()
        {
            var authors = db.tblMainAuthors.OrderBy(n => n.MainAuthorName).ToList();
            return PartialView(authors);
        }

        public ActionResult Publisher()
        {
            var publisher = db.tblPublishCompanies.OrderBy(n => n.PublisherName).ToList();
            return PartialView(publisher);
        }
        // product item - paritalview- right side
        public ActionResult Product(int? page)
        {
            int pageSize = 12;
            int pageNumber = (page ?? 1);
            var products = db.tblBooks.OrderBy(n => n.BookName).ToPagedList(pageNumber, pageSize);
            return View(products);
        }
        // show all product
        public ActionResult AllProduct()
        {
            return View();
        }
        // recommended product area
        public ActionResult RecommendedProduct(int? page)
        {
            int pageSize = 12;
            int pageNumber = (page ?? 1);
            var recommendedProducts = db.tblBooks.OrderBy(n => n.BookName).Where(n => n.Recommended == 1).ToPagedList(pageNumber, pageSize);
            return PartialView(recommendedProducts);
        }

        public ActionResult ProductDetail(int? masp )
        {
            tblBook sp = db.tblBooks.Where(n => n.BookID == masp).SingleOrDefault();
            return View(sp);
        }
        // Show up products by publisher
        public ActionResult ProductByPublisher(int? manxb)
        {
            IEnumerable<tblBook> sp = db.tblBooks.Where(n => n.PublisherID == manxb).AsEnumerable();
            return View(sp);
        }
        
        // show up  products by categories
        public ActionResult ProductByCategory(int? maDanhMuc)
        {
            IEnumerable<tblBook> sp = db.tblBooks.Where(n => n.CategoryID == maDanhMuc).AsEnumerable();
            return View(sp);
        }

        // Search box
        public ActionResult Search(string keyword)
        {
            var books = from b in db.tblBooks select b;
            if (!String.IsNullOrEmpty(keyword))
            {
                books = db.tblBooks.Where(b => b.BookName.Contains(keyword));
            }
            
            return View(books.ToList());
        }


    }
}