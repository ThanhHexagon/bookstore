﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookStore.Models;
using System.Data.Entity.Validation;
namespace BookStore.Controllers
{
    public class AccountController : Controller
    {
        private const string ACCOUNT = "Account";
        private const string USER_NAME = "Username";
        OnlineLibraryEntities db = new OnlineLibraryEntities();
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult LoginAccount()
        {
            return View();
        }

        public ActionResult Login(FormCollection f)
        {
            string username = f["txtLoginName"].ToString();
            string password = f["txtPassWord"].ToString();

            var user = db.tblCustomers.SingleOrDefault(n => n.CustomerUsername == username && n.CustomerPassword == password);
            if (user != null)
            {
                Session[ACCOUNT] = user.CustomerUsername;
                Session[USER_NAME] = user.CustomerPassword;
                return RedirectToAction("Index", "Product");
            }
            return RedirectToAction("LoginAccount", "Account");
        }

        public ActionResult Register(FormCollection f)
        {
            tblCustomer newUser = new tblCustomer();
            newUser.CustomerName = f["txtName"].ToString();
            newUser.CustomerUsername = f["txtUsername"].ToString();
            newUser.CustomerPassword = f["txtPassword"].ToString();
            //long id = db.tblCustomer.// table.Rows[table.Rows.Count - 1];
            if (ModelState.IsValid)
            {
                try
                {
                    ViewBag.TB = "Đăng ký thành công !";
                    db.tblCustomers.Add(newUser);
                    db.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    var errorMessages = ex.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                    // Join the list to a single string.
                    var fullErrorMessage = string.Join("; ", errorMessages);

                    // Combine the original exception message with the new one.
                    var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                    // Throw a new DbEntityValidationException with the improved exception message.
                    throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
                }
                
            }
            else
            {
                ViewBag.TB = "Đăng ký thất bại.";
            }
            return RedirectToAction("LoginAccount", "Account");
        }
    }
}