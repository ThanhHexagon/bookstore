//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BookStore.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblPublishCompany
    {
        public tblPublishCompany()
        {
            this.tblBooks = new HashSet<tblBook>();
        }
    
        public long PublisherID { get; set; }
        public string PublisherName { get; set; }
        public string PublisherPhone { get; set; }
        public string PublisherAddress { get; set; }
    
        public virtual ICollection<tblBook> tblBooks { get; set; }
    }
}
