﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Models
{
    public class CartItem
    {
        OnlineLibraryEntities db = new OnlineLibraryEntities();
        public long sID { get; set; }
        public string sBookName { get; set; }
        public float fPrice { get; set; }
        public int iQuantity { get; set; }
        public string sImage { get; set; }
        public float fTotal
        {
            get { return iQuantity * fPrice; }
        }

        public CartItem()
        {

        }
        public CartItem(int maSP, int quantity)
        {
            sID = maSP;
            tblBook book = db.tblBooks.Single(n => n.BookID == maSP);

            sBookName = book.BookName;
            fPrice = (float)book.Price;
            iQuantity = quantity;
            sImage = book.Image;
        }
    }
}