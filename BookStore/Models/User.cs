﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Models
{
    public class User
    {
        public string userID { get; set; }
        public string userName { get; set; }
        public string userLoginName { get; set; }
        public string userPassword { get; set; }
        public string userAddress { get; set; }
        public string userPhone { get; set; }


    }
}