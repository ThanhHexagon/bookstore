﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookStore.Models;
namespace BookStore.Areas.Admin.Controllers
{
    public class AdminController : Controller
    {
        OnlineLibraryEntities db = new OnlineLibraryEntities();
        // GET: Admin/Admin
        public ViewResult Index()
        {
            return View(db.tblBooks);
        }

        public ViewResult Edit(long productId)
        {
            tblBook product = db.tblBooks.FirstOrDefault(p => p.BookID == productId); 
            return View(product); 
        }
    }
}